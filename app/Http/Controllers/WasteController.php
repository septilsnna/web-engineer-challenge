<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Waste;
use Illuminate\Support\Facades\Validator;

class WasteController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wastes = Waste::all();


        return $this->sendResponse($wastes->toArray(), 'Wastes retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


        $validator = Validator::make($input, [
            'nama_sampah' => 'required',
            'kategori_sampah' => 'required'
        ]);


        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


        $waste = Waste::create($input);


        return $this->sendResponse($waste->toArray(), 'Waste created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Waste $waste)
    {
        $waste->delete();


        return $this->sendResponse($waste->toArray(), 'Waste deleted successfully.');
    }
}