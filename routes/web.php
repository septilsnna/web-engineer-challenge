<?php

use Illuminate\Support\Facades\Route;
use App\Models\Waste;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $wastes = Waste::all();

    return view('data_sampah', compact('wastes'));
});

Route::get('/tambah', function () {
    return view('input_sampah');
});

Route::post('/tambah_data', function (Request $request) {
    $nama_sampah = $request->old('nama_sampah');
    $kategori_sampah = $request->old('kategori_sampah');

    // form validation
    $request->validate([
        'nama_sampah' => 'required|unique:wastes,nama_sampah',
        'kategori_sampah' => 'required',
    ]);

    $waste = new Waste;
    $waste->nama_sampah = $request->nama_sampah;
    $waste->kategori_sampah = $request->kategori_sampah;

    $waste->save();

    return redirect('/');
});

Route::get('/hapus/{id}', function ($id) {
    $waste = Waste::find($id);
    $waste->delete();
    return redirect('/');
});