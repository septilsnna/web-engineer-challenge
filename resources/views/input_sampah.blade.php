<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Input Sampah</title>
</head>

<body style="background-color: #eef5f6; font-family: 'Montserrat', sans-serif;">
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #0c325f;">
        <h5 class="mx-auto my-auto navbar-brand px-2" style="color: #ffffff; font-size:16px">Input Sampah</h5>
    </nav>

    <div class="container my-3">
        <form action="/tambah_data" method="post">
            @csrf
            <div class="form-group">
                <label for="kategori_sampah" style="color: gray; font-size: 14px;">Kategori Sampah</label>
                <select class="form-control" id="kategori_sampah" name="kategori_sampah"
                    style="border-radius: 10px;font-size:16px;">
                    <option selected>Kategori</option>
                    <option value="Kaca">Kaca</option>
                    <option value="Plastik">Plastik</option>
                    <option value="Kertas">Kertas</option>
                    <option value="Logam">Logam</option>
                    <option value="Tekstil">Tekstil</option>
                    <option value="Barang Elektronik">Barang Elektronik</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nama_sampah" style="color: gray; font-size:14px;">Nama Sampah</label>
                <input type="text" class="form-control" id="nama_sampah" name="nama_sampah"
                    style="border-radius: 10px;font-size:16px;">
            </div>
            <button type="button submit" class="btn btn-block my-3 py-3"
                style="background-color:#30aee4; color: #ffffff; border-radius: 10px;font-size:16px;">Simpan</button>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>