<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Data Sampah</title>
</head>

<body style="background-color: #eef5f6; font-family: 'Montserrat', sans-serif;">
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #0c325f;">
        <h5 class="mx-auto my-auto navbar-brand px-2" style="color: #ffffff; font-size: 16px;">Data Sampah</h5>
    </nav>

    <div class="container my-3">
        <a href="/tambah" class="btn btn-block mb-3 py-3 shadow"
            style="background-color:#30aee4; color: #ffffff; border-radius: 10px;font-size: 16px;">Tambah</a>
        @foreach($wastes as $d)
        <div class="card shadow mb-3 bg-white" style="border-radius: 10px;">
            <div class="card-body">
                <h5 style="font-size: 16px;">{{ $d['nama_sampah']}}</h5>
                <div class="row">
                    <div class="col">
                        <h6 style="font-size: 14px; color:gray">{{ $d['kategori_sampah']}}</h6>
                    </div>
                    <div class="col offset-8">
                        <a href="/hapus/{{ $d['id']}}" Style="font-size: 14px; color:red">Hapus</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>